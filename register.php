<?php
    require("common.php");
?>

<script>
function checkRegister() {
    $user = $("#txtUser").val();
    $pwd = $("#txtPass").val();
    $email = $("#txtEmail").val();
    $emailConfirm = $("#txtEmailConfirm").val();
    if ($email.trim().length == 0 || $email != $emailConfirm) {
        $("#divResult").html("<span style='color:red;'>Email not identical</span>");
        return;
    }

    $.ajax({url: "checkRegister.php", 
        type: "POST",
        data:{
            username:$user,
            pwd:$pwd,
            email:$email
        },
        success: function(result){
            $("#divResult").html(result);
        }
    });
}

//-----------------START EVENT----------------
$(document).ready(function(){
    $("#btnRegister").click(function(){
        checkRegister();
    });
});

//-----------------END EVENT----------------
</script>
User Id <input type="text" name="txtUser" id="txtUser"/></br>
Password <input type="password" name="txtPass" id="txtPass"/></br>
Email <input type="email" name="txtEmail" id="txtEmail"/></br>
Email Confirmation<input type="email" name="txtEmailConfirm" id="txtEmailConfirm"/></br>
<input type="button" id="btnRegister" value="Register" /></br>

<div id="divResult" id ="divResult"></div>
